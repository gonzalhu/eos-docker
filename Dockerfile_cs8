#
# EOS Docker file for CentOS Stream 8
#
# Version 0.1

FROM gitlab-registry.cern.ch/linuxsupport/cs8-base
LABEL maintainer="Fabio Luchetti, fabio.luchetti@cern.ch, CERN 2021"

ARG EOS_CODENAME

# Expect to use eos-xrootd in running containers
ENV PATH /opt/eos/xrootd/bin:$PATH
ENV LD_LIBRARY_PATH /opt/eos/xrootd/lib64:$LD_LIBRARY_PATH

# Add extra repositories
COPY eos-docker/el-8s/*.repo /etc/yum.repos.d/

# Add helper scripts
COPY eos-docker/image_scripts/*.sh /

# Add configuration files for EOS instance
COPY eos-docker/eos.sysconfig /etc/sysconfig/eos
COPY eos-docker/xrd.cf.* eos-docker/krb5.conf /etc/
COPY eos-docker/fuse.eosdockertest.conf /etc/eos/fuse.eosdockertest.conf
COPY eos-docker/fuse.conf /etc/eos/fuse.mount-1.conf
COPY eos-docker/fuse.conf /etc/eos/fuse.mount-2.conf

# Add configuration files for forwarding proxy server
COPY eos-docker/xrootd.conf /etc/tmpfiles.d/
COPY eos-docker/xrootd-fwd-proxy.cfg /etc/xrootd/

# Set correct path for the dependency repository based on the EOS version
# which is being built
RUN sed -i "s/__EOS_CODENAME__/${EOS_CODENAME}/g" /etc/yum.repos.d/eos.repo

RUN mkdir /var/tmp/eosxd-cache/ /var/tmp/eosxd-journal/

RUN adduser eos-user && adduser eosnobody || true

# Docker will aggressively cache the following command, but this is fine, since
# these packages are not updated often.
RUN dnf -y install epel-release dnf-plugins-core \
    && dnf -y --nogpg install at autoconf automake bzip2 cmake createrepo \
           gcc-c++ gdb git glibc-all-langpacks initscripts krb5-server \
           krb5-workstation less libgfortran libtool parallel \
           perl-Test-Harness python2 python2-pip python3 python3-pip \
           redhat-lsb-core redis rpm-build sudo xauth davix openssl rsync \
    && dnf -y --nogpg install https://repo.opensciencegrid.org/osg/3.6/osg-3.6-el8-release-latest.rpm \
    && dnf -y --nogpg install x509-scitokens-issuer-client --enablerepo=osg-development \
    && dnf -y --nogpg remove osg-release \
    && dnf clean all

# Install EOS from locally created repo
# Note: the ADD command will reset the docker cache
# and any commands after that point will be uncached
ENV EOSREPODIR="/repo/eos"
ADD cs8_artifacts ${EOSREPODIR}

# Special packages, must be installed un-cached
RUN createrepo ${EOSREPODIR}; \
    echo -e "[eos-artifacts]\nname=EOS artifacts\nbaseurl=file://${EOSREPODIR}\ngpgcheck=0\nenabled=1\npriority=1" >> /etc/yum.repos.d/eos.repo \
    && dnf -y --nogpg install eos-server eos-client eos-archive eos-fusex \
                              eos-ns-inspect eos-test eos-testkeytab \
    && dnf clean all

RUN if [ "${EOS_CODENAME}" != "diopside" ]; \
    then dnf -y --nogpg install eos-fuse quarkdb; \
    else dnf -y --nogpg install eos-quarkdb; fi

# The system tests have a strong bias towards nobody having uid=99 guid=99
RUN usermod -u 99 nobody && groupmod -g 99 nobody

# Create macaroon secret and set sss keytab permissions to400
RUN openssl rand -base64 -out /etc/eos.macaroon.secret 64 \
    && chown 2:2 /etc/eos.macaroon.secret \
    && chmod 400 /etc/eos.keytab /etc/eos.client.keytab /etc/eos.macaroon.secret

# Generate only a common root CA certificate
RUN /mkcert-ssl.sh -s
ENTRYPOINT ["/bin/bash"]
