#!/usr/bin/env bash

# Preload jemalloc
jemalloc=$(find /usr/lib64/ -name "libjemalloc*" | sort | tail -1)

if [[ ! -z "${jemalloc}" ]]; then
  export LD_PRELOAD=${jemalloc}
fi

if [ -e /opt/eos/xrootd/bin/xrootd ]; then
   XROOTDEXE="/opt/eos/xrootd/bin/xrootd"
else
   XROOTDEXE="/usr/bin/xrootd"
fi

quarkdb-create --path /var/quarkdb/node-0
chown -R daemon:daemon /var/quarkdb/node-0
${XROOTDEXE} -n qdb -c /etc/xrd.cf.quarkdb -l /var/log/eos/xrdlog.qdb -b -Rdaemon
