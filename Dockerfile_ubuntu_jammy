# Simple EOS Docker file
#
# Version 0.1

FROM ubuntu:jammy
LABEL maintainer="Elvin Sindrilaru esindril@cern.ch, CERN 2022"

ARG EOS_CODENAME

# Add helper scripts
COPY eos-docker/image_scripts/*.sh /

# Add configuration files for EOS instance
COPY eos-docker/eos.sysconfig /etc/sysconfig/eos
COPY eos-docker/xrd.cf.* eos-docker/krb5.conf /etc/
COPY eos-docker/fuse.eosdockertest.conf /etc/eos/fuse.eosdockertest.conf
COPY eos-docker/fuse.conf /etc/eos/fuse.mount-1.conf
COPY eos-docker/fuse.conf /etc/eos/fuse.mount-2.conf

RUN mkdir /var/tmp/eosxd-cache/ /var/tmp/eosxd-journal/

RUN adduser eos-user && adduser eosnobody

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update \
    && apt-get install -y software-properties-common \
    && apt-get install -y --no-install-recommends apt-utils \
    && apt-get install -y at autoconf automake build-essential bzip2 \
    cmake cpio rpm2cpio curl fort77 gcc gdb gdebi-core gfortran git g++ \
    heimdal-servers krb5-admin-server krb5-kdc krb5-user libpam-ccreds \
    libpam-krb5 libtool parallel perl redis rsync wget python3 python3-pip \
    sqlite3 \
    && apt-get clean

# Setup /usr/bin/python otherwise only works if specifying python3 explicitly
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 1

# Add XRootd repo
# @todo once the XRootD deb repos are better structured we can point to a
# particular stable branch. For the moment, we force install the version we want
# Priority >= 1000 causes a version to be installed even if this constitutes a downgrade of the package
RUN curl -sL http://storage-ci.web.cern.ch/storage-ci/storageci.key | apt-key add - \
    && add-apt-repository "deb http://storage-ci.web.cern.ch/storage-ci/debian/xrootd $(lsb_release -sc) release"

RUN if [ "${EOS_CODENAME}" = "diopside" ]; then export XRDVERSIONPIN=5.5.5; \
    elif [ "${EOS_CODENAME}" = "citrine" ]; then export XRDVERSIONPIN=4.12.8; \
    else echo "Error: EOS_CODENAME:'${EOS_CODENAME}' not supported, exiting" && exit -1; fi \
    && echo "Package: xrootd* libxrd* libxrootd*\nPin: version ${XRDVERSIONPIN}\nPin-Priority: 1000" > /etc/apt/preferences.d/xrootd.pref

# Create local repo from eos artifacts
ENV EOSREPODIR="/debs/eos/"
ADD jammy_artifacts ${EOSREPODIR}

RUN cd ${EOSREPODIR} \
    && dpkg-scanpackages . > Packages \
    && gzip --keep -9 Packages \
    && echo "deb [trusted=yes] file:${EOSREPODIR} ./\n" >> /etc/apt/sources.list \
    && apt-get update \
    && apt-get install -y eos-client eos-fusex eos-test eos-testkeytab \
    && apt-get clean

# sss keytab needs to be 400
RUN chmod 400 /etc/eos.client.keytab
ENV DEBIAN_FRONTEND default

# Generate only a common root CA certificate
RUN /mkcert-ssl.sh -s
ENTRYPOINT ["/bin/bash"]
